<!DOCTYPE html>

<html>
     <head>

        <title>BD - RÉGUAS</title>

        <meta charset="utf-8">

        <meta name="viewport" content="width=500, initial-scale=1">
        <link href="css/style_home.css" type="text/css" rel="stylesheet">
        <link href="bootstrap-4.3.1-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="jquery-3.4.1.js" type="text/javascript" rel="stylesheet">
        
    </head>
    
    <body>

        <header>
            <!-- TOP DA PÁGINA-->
            
            <h2>Réguas - Layout</h2>
        </header>
        
        <br>

            <form method="POST" action="home_.php">
             
                    <!-- MENU FILTRO H3 TÍTULO (A1) -->
                    <h4>
                        <strong>Filtrar resultados por:</strong>
                    </h4>
                    <hr id="bordhr">
                   

                    <!-- CARREGARÁ AS PALAVRAS FILTRADAS (A1) 
                    <div id="filters" >

                        <div class="add_filtro" id="regua_filtro">Régua <img src="xbutton.ico" width="10px" class="img_xA" onclick="fecharRegua()"></div>
                        <div class="add_filtro" id="motor_filtro" >Motor <img src="xbutton.ico" width="10px"
                                                                              class="img_xB" onclick="fecharMotor()"></div>
                        <div class="add_filtro" id="canal_filtro">Canal <img src="xbutton.ico" width="10px" class="img_xC" onclick="fecharCanal()">
                        </div>

                        <div class="add_filtro" id="fase_filtro"> Fase <img src="xbutton.ico" width="10px" class="img_xD" onclick="fecharFase()">
                        </div>

                        <div class="add_filtro" id="vigencia_filtro">Vigência <img src="xbutton.ico" width="10px" class="img_xE" onclick="fecharVigencia()"></div>

                    </div>

                    
                    <!-- Caixa de pesquisa (A1) 

                    <div id="divBusca">
                        <input type="text" id="txtBusca" placeholder="Digite...">
                        <img src="img/icon_2.png" id="btnBusca" alt="Buscar">

                    </div>

                    <div class="but1">
                        <button type="submit" value="enviar" id="buttone">Ok</button>
                    </div>

                    <div class="but2">
                        <button type="reset" value="limpar" id="buttonl" onclick="fecharTudo()">Limpar</button>
                    </div>  -->  

                    <!-- Essa DIV Controla os checkbox (A1) -->
                    
                    <div class="checkboxes">
                        
                        <h5><strong>Réguas</strong></h5>
                        
                        <input type="checkbox" id="cartao" data-url="home_" name="cart">
                        <label for="regua">CARTÃO</label>
                        <br>
                        <input type="checkbox" id="consorcio" data-url="select_conso" name="conso">
                        <label for="motor">CONSÓRCIO</label>
                        <br>
                        <input type="checkbox" id="seguros" data-url="select_seguro" name="seguro">
                        <label for="canal">SEGUROS</label>
                        <br>
                        <input type="checkbox" id="credito" data-url="select_credito" name="cred">
                        <label for="fases">CREDITO</label>
                        <br>
                        <input type="checkbox" id="investimentos" data-url="select_investimento" name="invest">
                        <label for="vigencia">INVESTIMENTO</label>
                        <br>
                        <input type="checkbox" id="imobiliario" data-url="select_imobiliario" name="imob">
                        <label for="vigencia">IMOBILIARIO</label>
                        <br>
                        <input type="checkbox" id="capitalizacao" data-url="select_capitalizacao" name="capital">
                        <label for="vigencia">CAPITALIZAÇÃO</label>
                        <br>
                        <input type="checkbox" id="previdencia" data-url="select_previdencia" name="previ">
                        <label for="vigencia">PREVIDÊNCIA</label>
                        <br>
                        
                        <div><a href="#"><strong>Mostrar + 10</strong></a></div>
                      
                        <hr class="hr2"> 
                        
                        <h5><strong> Fase </strong></h5>
                        
                        <input type="checkbox" id="atracao_b" data-url="select_atracaob" name="abv">
                        <label for="regua">ATRAÇÃO/BOAS VINDAS</label>
                        <br>
                        <input type="checkbox" id="fidel_D" data-url="select_fidelizacao" name="fd">
                        <label for="regua">FIDELIZAÇÃO/DURANTE</label>
                        <br>
                        <input type="checkbox" id="rete_p" data-url="select_retencao" name="rp">
                        <label for="regua">RETENÇÃO/PÓS</label>
                        
                        <h5><strong> Canal </strong></h5>
                        
                        <input type="checkbox" id="" data-url="select_notificacao" name="notifi">
                        <label for="regua">NOTIFICAÇÃO - NT</label>
                        <br>
                        <input type="checkbox" id="" data-url="select_sms" name="sms">
                        <label for="regua">SMS</label>
                        <br>
                        <input type="checkbox" id="" data-url="select_emailmkt" name="mkt">
                        <label for="regua">EMAIL MKT - EM</label>
                        <br>
                        <input type="checkbox" id="" data-url="select_falecom" name="fale">
                        <label for="regua">FALE COM </label>
                        <br>
                        <input type="checkbox" id="" data-url="select_taa" name="taa">
                        <label for="regua">TAA</label>
                        <br>
                        <input type="checkbox" id="" data-url="select_crbb" name="crbb">
                        <label for="regua">CRBB</label>
                        <br>
                        <input type="checkbox" id="" data-url="select_consultor" name="consult">
                        <label for="regua">CONSULTOR</label>
                        <br>
                        <input type="checkbox" id="" data-url="select_plataforma" name="plata">
                        <label for="regua">PLATAFORMA</label>
                       <div><a href="#"><strong>Mostrar + 7</strong></a></div>

                        <hr class="hr2">
                        
                        <h5><strong> Situação </strong></h5>
                        
                        <input type="checkbox" id="" name="vige">
                        <label for="regua">VIGENTE</label>
                        <br>
                        <input type="checkbox" id="" name="nvige">
                        <label for="regua">NÃO VIGENTE</label>
                        <br>
                        <input type="checkbox" id="" name="eimplant">
                        <label for="regua">EM IMPLANTAÇÃO</label>
                        
                        <hr class="hr2">
                        
                        <h5><strong> Formato </strong></h5>
                        
                        <input type="checkbox" id="" name="motoron">
                        <label for="regua">MOTOR ON LINE</label>
                        <br>
                        <input type="checkbox" id="" name="camp">
                        <label for="regua">CAMPANHAS </label>
                        <br>
                        <input type="checkbox" id="" name="outras">
                        <label for="regua">OUTRAS </label>
                        
                        <hr class="hr2">
                        
                        <h5><strong> Efetividade </strong></h5>
                        
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">  ACIMA DE 30%</label>
                        <br>
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">ACIMA DE 20%</label>
                        <br>
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">ACIMA DE 10% </label>
                        <br>
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">ACIMA DE 5% </label>
                        <br>
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">ACIMA DE 1% </label>
                        <br>
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">MENOS DE 1% </label>
                        
                        <hr class="hr2">
                        
                        <h5><strong> Outros Filtros </strong></h5>
                        
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">  ENCARTEIRADOS</label>
                        <br>
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">NÃO ENCARTEIRADOS</label>
                        <br>
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">VAREJO </label>
                        <br>
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">ESTILO </label>
                        <br>
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">PERSONALIZADO </label>
                        <br>
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">UNIVERSITÁRIOS </label>
                        <br>
                        <input type="checkbox" id="" name="checks">
                        <label for="regua">NATIVOS DIGITAL </label>
                        <div><a href="#"><strong>Mostrar mais + 7</strong></a></div>

                    </div> 
            </form>
        
        <!-- FLEXBOX-->
        
            <div class="container">
               
            </div>
                       <!-- <span><button>Detalhes</button></span>        
                </div>        -->
        
<script src="jquery-3.4.1.js"></script> 
                       
    <script type="text/javascript">
        
        $("input[type=checkbox]").on("change", function () { // <- AO ALTERAR QUALQUER INPUT TIPO CHECKBOX.. 
            if ($(this).prop("checked") === true) { 
                var ms = ($(this).prop("checked") === true);      // <- ELE VERIFICARÁ SE OS 'THIS'(NO CASO, OS CHECKBOXES) ESTÃO MARCADOS..
                /*var msg = $(ms).prop("propertyName");  */         // VAR MS E MSG TENTO RETORNAR O NOME DA CHECKBOX MARCADA NO ALERT, MAS N CONSEGUI
               
                checkbox1 = $("input[type=checkbox][name=cart]").val();
                                                             //<- E GUARDARÁ SEUS VALORES DENTRO DE UMA VARIAVEL PUXADOS PELA SUA TAG NAME.
                console.log(checkbox1);                      //DA FORMA COMO ESTÁ ELE RETORNA "on", MAS É POSSIVEL ADICIONAR VALORES AOS CHECKBOX COM VALUE="" PARA ARMAZENAR.
                                                             //NA CHECKBOX SEGUROS ADICIONEI VALUE="130" NA QUAL FOI ARMAZENADO NA VARIAVEL CHECK3, MAS PODERIA SER QUALQUER VALOR.
                checkbox2 = $("input[type=checkbox][name=conso]").val();

                console.log(checkbox2);                      //ESSES CONSOLES SAO SO PARA QUE VEJA OS VALORES NO CONSOLE DO NAVEGADOR
                                                             // COM OS VALORES PUXADOS POR NAME E ARMAZENADOS EM VAR'S FALTA SO PUXALOS COMO DESEJAR.
                checkbox3 = $("input[type=checkbox][name=seguro]").val();

                console.log(checkbox3);

                checkbox4 = $("input[type=checkbox][name=cred]").val();

                console.log(checkbox4);          // <- NECESSÁRIO CRIAR VARIAVEIS PARA ARMAZENAR E PUXAR OS VALORES DE TODAS AS CHECK.

                ($(this).prop("checked") === true).val;
                
                $.ajax({
                    url: "php/" + this.dataset.url + ".php",
                    data: { cart: checkbox1, conso: checkbox2, seguro: checkbox3, cred: checkbox4 },
                    success: function (data) {
                        $('.container').html(data);
                        $('.container').show();
                        $.each(data, function (i, element) {
                            $('.container').html(element.linha_usuario);
                        });
                    }
                });

            }
            else {
               console.log("caixa desmarcada");

            }

        });

    </script>
    </body>
</html>